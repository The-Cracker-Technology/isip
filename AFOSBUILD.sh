rm -rf /opt/ANDRAX/isip

source /opt/ANDRAX/PYENV/python2/bin/activate

/opt/ANDRAX/PYENV/python2/bin/pip2 install -r requirements.txt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

cp -Rf isip /opt/ANDRAX/isip/

chown -Rf andrax:andrax /opt/ANDRAX
chmod -Rf 755 /opt/ANDRAX
